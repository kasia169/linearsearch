package K.AdamczykLineraSearch;

import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class BinarySearch<T extends  Comparable<T>> extends Search<T> {

    @Override
    public int doSearch(List<T> list, T searchElement) {
        return binarySearch(list, 0, list.size() - 1, searchElement);
    }

    public int binarySearch(List<T> list, int start, int end, T searchElement) {

        int middle = (start + end) / 2;

        if (start > end) {
            return -1;
        }

        if (list.get(middle).compareTo(searchElement) == 0) {
            return middle;
        }

        return list.get(middle).compareTo(searchElement) > 0 ?
                binarySearch(list, start, middle - 1, searchElement) :
                binarySearch(list, middle + 1, end, searchElement);
    }
}
