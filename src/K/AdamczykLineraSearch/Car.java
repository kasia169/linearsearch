package K.AdamczykLineraSearch;

/**
 * Created by RENT on 2017-08-18.
 */
public class Car implements Comparable<Car> {
    private String model;
    private String marka;
    private String color;

    public Car(String model, String marka, String color) {
        this.model = model;
        this.marka = marka;
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @Override
    public int compareTo(Car car) {

        int result = -1;

        if (getMarka() == car.getMarka() && getModel() == car.getModel()) {
            result = 0;
        }
            return result;
    }

    public String toString(){
        return ""  +  marka + " " + model + " " + color;
    }
}



